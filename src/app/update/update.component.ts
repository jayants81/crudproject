import { Component, OnInit } from '@angular/core';
import { MatDialogRef,MatDialog} from '@angular/material/dialog';
import { HttpClient } from '@angular/common/http';
import { Router,ActivatedRoute } from '@angular/router';
import { DataService } from '../data.service';
// import {Router} from '@angular/router';
import {User} from '../model/user.modal';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {first} from 'rxjs/operators';

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.less']
})
export class UpdateComponent implements OnInit {
title: string;
body:string;
user = {};
editForm: FormGroup;

// tslint:disable-next-line: max-line-length
constructor(private formBuilder: FormBuilder, private router: Router, private apiService: DataService, private http: HttpClient, private route: ActivatedRoute) {
      // this.route.params.subscribe(param => {
      //   if (param['id']) {
      //     this.openDialog2(param['id']);
      //   }
      // });
    }

  ngOnInit() {
    let userId = window.localStorage.getItem('editUserId');
    if(!userId) {
      alert('Invalid action.')
      this.router.navigate(['/users']);
      return;
    }
    this.editForm = this.formBuilder.group({
      id: userId,
      title: ['', Validators.required],
      body: ['', Validators.required],

    });
    this.apiService.getUserById(+userId)
      .subscribe( data => {
        this.editForm.setValue(data.result);
      });
  }
  // openDialog2(id:string){

  //   this.http.get('https://jsonplaceholder.typicode.com/posts/' + id).subscribe((res:any) =>{
  //   this.user = res;
  //     console.log(id);


  //   });
  // }

  onSubmit() {
    this.apiService.updateUser(this.editForm.value)
      .pipe(first())
      .subscribe(
       ( res : any) => {
          if(res) {
            alert('User updated successfully.');
            this.router.navigate(['/users']);
            this.apiService.getList();
          } else {
            alert(res.message);
          }
        },
        error => {
          alert(error);
        });
  }


}
