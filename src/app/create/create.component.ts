import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef,MatDialog} from '@angular/material/dialog';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { DataService } from '../data.service';

import {FormBuilder, FormGroup, Validators} from '@angular/forms';
// import {Router} from '@angular/router';
// import {DataService} from '../data.service';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.less']
})
export class CreateComponent implements OnInit {
  model: any = {};

  constructor(private router: Router,private dataService: DataService,private formBuilder: FormBuilder,
    private http: HttpClient, public dialogRef: MatDialogRef<CreateComponent>,public dialog: MatDialog) { }
    addForm: FormGroup;
  ngOnInit() {
    this.addForm = this.formBuilder.group({
          id: [],
          title: ['', Validators.required],
          body: ['', Validators.required],

        });

      }

      onSubmit() {
        this.dataService.createUser(this.addForm.value)
          .subscribe( data => {
            this.router.navigate(['users']);
          });
      }
  onNoClick(): void {
    this.dialogRef.close();
  }
//   onSubmit() {

//     this.http.post('https://jsonplaceholder.typicode.com/posts', this.model)
//       .subscribe((res: any) => {
//         console.log(res);
//         alert('User is created')
//       });
// }

// constructor(private formBuilder: FormBuilder, private router: Router, private apiService: DataService) { }

// addForm: FormGroup;

// ngOnInit() {
//   this.addForm = this.formBuilder.group({
//     id: [],
//     username: ['', Validators.required],
//     password: ['', Validators.required],
//     firstName: ['', Validators.required],
//     lastName: ['', Validators.required],
//     age: ['', Validators.required],
//     salary: ['', Validators.required]
//   });

// }

// onSubmit() {
//   this.apiService.createUser(this.addForm.value)
//     .subscribe( data => {
//       this.router.navigate(['list-user']);
//     });
// }



}
