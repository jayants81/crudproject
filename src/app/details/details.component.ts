import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Router,ActivatedRoute} from '@angular/router';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import { DataService } from '../data.service';


@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.less']
})
export class DetailsComponent implements OnInit {
user = {};
comment = [];
  constructor(private dataService: DataService, private http: HttpClient, private route: ActivatedRoute) {
     this.route.params.subscribe(param => {
       if (param['id']) {
         this.getDetails(param['id']);
       }
     });
     }

  ngOnInit() {
  }
  getDetails(id:string){

    this.http.get('https://jsonplaceholder.typicode.com/posts/' + id).subscribe((res:any) =>{
    this.user = res;
    this.http.get('https://jsonplaceholder.typicode.com/posts/' + id + '/comments').subscribe((res:any) => {
      this.comment = res;
    });
      console.log(id);


    });
  }
}
