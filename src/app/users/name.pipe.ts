import { Pipe, PipeTransform} from '@angular/core';


@Pipe({
  name :'name'
})
export class NamePipe implements PipeTransform {
  transform(login : string ):any{
    let name = login;

    let fWord ="";
    if(name){
      fWord =  name.split('')[0].toUpperCase();
    }
    return fWord;
  }
}


