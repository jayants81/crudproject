import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute, ParamMap} from '@angular/router';
import { Observable } from 'rxjs';
import { HttpClient} from '@angular/common/http';
import { DataService } from '../data.service';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { CreateComponent } from '../create/create.component';
// import { UpdateComponent } from '../update/update.component';

// import {Router} from '@angular/router';
// import {User} from '../model/user.modal';
//import {DataService} from '../data.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.less']
})
export class UsersComponent implements OnInit {
  list = [];
  name = 'Angular';
  user =  {};
  update = {};
  listObservable: Observable<any[]> ;
  constructor(private dataService: DataService, private router:Router,
               private http: HttpClient , private route: ActivatedRoute, public dialog: MatDialog) { }

  ngOnInit() {
    this.dataService.getList().subscribe((res: any[]) => {

          this.list = res;
    });

  }

getDetails(id: string){

    this.http.get('https://jsonplaceholder.typicode.com/posts/' + id).subscribe((res: any) =>{
      this.router.navigateByUrl('/user/' + id);
      console.log(id);


    });
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(CreateComponent, {
      width: '650px',
      height : '550px'
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');

    });
  }

  openDialog2(id: string): void {
    this.http.get('https://jsonplaceholder.typicode.com/posts/' + id).subscribe((res: any) => {
      this.router.navigateByUrl('/update/' + id);
      console.log(id);
      console.log(res);
    });
  }

  editUser(list) {
      window.localStorage.removeItem('editUserId');
      window.localStorage.setItem('editUserId', list.id.toString());
      this.router.navigate(['update']);
    };

// users: User[];

// constructor(private router: Router, private apiService: DataService) { }

// ngOnInit() {
//   // if(!window.localStorage.getItem('token')) {
//   //   this.router.navigate(['login']);
//   //   return;
//   // }
//   this.apiService.getUsers()
//     .subscribe( data => {
//         this.users = data.result;
//     });
// }

// deleteUser(user: User): void {
//   this.apiService.deleteUser(user.id)
//     .subscribe( data => {
//       this.users = this.users.filter(u => u !== user);
//     })
// };

// editUser(user: User): void {
//   window.localStorage.removeItem('editUserId');
//   window.localStorage.setItem('editUserId', user.id.toString());
//   this.router.navigate(['edit-user']);
// };

// addUser(): void {
//   this.router.navigate(['create']);
// };

}
