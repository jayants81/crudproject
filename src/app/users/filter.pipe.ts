// import { Pipe, PipeTransform } from '@angular/core';
// // import {UsersComponent} from '../users/users.component'
// @Pipe({
//   name: 'filter'
// })
// export class FilterPipe implements PipeTransform {

//   transform(list: any[], searchText: string): any[] {
//     if(!list) return [];

//     if(!searchText) return list;
//       searchText = searchText.toLowerCase();

//       return list.filter( it => {
//       return it.login.toLowerCase().includes(searchText);

//     });
//    }
// }

import { Pipe, PipeTransform} from '@angular/core';


@Pipe({
  name : 'filter'
})
export class FilterPipe implements PipeTransform {
  transform(list: any[], searchText: string) : any[] {
    if(!list) return [];

    if(!searchText) return list;
    searchText = searchText.toLowerCase();
    return list.filter( it => {
      return it.login.toLowerCase().includes(searchText);
    })
  }
}
