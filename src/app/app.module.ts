import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import {ReactiveFormsModule} from "@angular/forms";

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {MatCardModule} from '@angular/material/card';
import { DetailsComponent } from './details/details.component';
import { UsersComponent } from './users/users.component';
import { FilterPipe} from './users/filter.pipe';
import { NamePipe } from './users/name.pipe';
import {MatDialogModule} from '@angular/material/dialog';
import { CreateComponent } from './create/create.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import { UpdateComponent } from './update/update.component';

@NgModule({
  declarations: [
    AppComponent,
    DetailsComponent,
    UsersComponent,
    FilterPipe,
    NamePipe,
    CreateComponent,
    UpdateComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    MatCardModule,
    FormsModule,
    MatDialogModule,
    MatFormFieldModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [CreateComponent,UpdateComponent]
})
export class AppModule { }
