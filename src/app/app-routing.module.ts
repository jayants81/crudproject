import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatButtonModule, MatCheckboxModule} from '@angular/material';
import { HttpClientModule } from '@angular/common/http';
import { DetailsComponent} from './details/details.component'
import { UsersComponent } from './users/users.component';
import { UpdateComponent } from './update/update.component';

const routes: Routes = [
  {path:'',  redirectTo:'users', pathMatch : 'full'},
 {path : 'user/:id', component : DetailsComponent},
 {path : 'users', component : UsersComponent},
 {path : 'update', component : UpdateComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes),
    BrowserAnimationsModule,HttpClientModule,
    MatButtonModule, MatCheckboxModule],
  exports: [RouterModule]
})
export class AppRoutingModule { }
