import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute, ParamMap} from '@angular/router';
import { Observable } from 'rxjs';
import { HttpClient} from '@angular/common/http';
import { DataService } from './data.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent implements OnInit {
    Details = [];
    response: Observable<any>;
  constructor(private dataService: DataService, private router:Router, private http : HttpClient,private route: ActivatedRoute,){

  }

    ngOnInit() {

    }


    request() {
      const url = 'https://jsonplaceholder.typicode.com/posts/1';
      this.response = this.http.get(url, {observe: 'body'});
  }





}
