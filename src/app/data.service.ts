import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Router} from '@angular/router';
import { Observable, of } from 'rxjs';
import {ApiResponse} from './model/api.response';
import {User} from './model/user.modal';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  baseUrl:string = 'https://jsonplaceholder.typicode.com/posts';
  update:string = 'https://jsonplaceholder.typicode.com/posts/';
  list = [];

  constructor(private http: HttpClient, private router: Router) { }

  getList() {
  return   this.http.get(this.baseUrl);
  }
  createUser(list): Observable<ApiResponse> {
     return this.http.post<ApiResponse>(this.baseUrl, list);
  }
  getUserById(id: number): Observable<ApiResponse> {
     return this.http.get<ApiResponse>(this.update   + id);
  }

  updateUser(list): Observable<ApiResponse> {
     return this.http.put<ApiResponse>(this.update  + list.id, list);
  }



  }




